#!/usr/bin/env node

const operations = {
  'generate-private-key': () => {
    const { PrivateKey } = require('./lib/brd-node');

    this.console.log((new PrivateKey()).toString());
  },
};

const op = process.argv[2];
const args = process.argv.slice(3);

if (operations[op]) operations[op].apply(this, args);
else {
  this.console.log('unknown operation: ', op);
  this.console.log(' try - partner-connector generate-private-key');
}
