module.exports = {
  // standard screen types:
  warning(hash) {
    return {
      type: 'warning',
      ...hash,
    };
  },
};
