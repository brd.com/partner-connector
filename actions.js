module.exports = {
  browserURL(hash) {
    if (!hash.url) throw new Error('Must specify a URL');

    return {
      type: 'browser-url',
      method: 'GET',
      closeOn: `${process.env.BASE_URL || ''}/_close`,
      ...hash,
    };
  },
  flow(title, name) {
    return {
      type: 'flow',
      title,
      name,
    };
  },
  quote(hash) {
    return {
      type: 'quote',
      params: hash.params,
      title: hash.title || 'Update Quote',
      style: hash.style || 'normal',
    };
  },
  navigate(hash) {
    if (!hash.title) throw new Error('Must specify a title');
    if (!hash.url) throw new Error('Must specify a URL');

    return {
      type: 'navigate',
      url: hash.url,
      title: hash.title,
      style: hash.style || 'normal',
    };
  },
  close(hash) {
    if (!hash.title) throw new Error('Must specify a title');

    return {
      type: 'close',
      title: hash.title,
      style: hash.style || 'normal',
    };
  },
  rpc(method, params, options = {}) {
    return {
      type: 'rpc',
      method,
      params,
      ...options,
    };
  },
  transaction(hash) {
    if (!hash.amount) throw new Error('Must specify an amount');
    if (!hash.address) throw new Error('Must specify an address');
    if (!hash.currency) throw new Error('Must specify an address');

    return {
      type: 'blockchain-tx',
      ...hash,
    };
  },
};
