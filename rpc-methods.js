const cryptoRandomString = require('crypto-random-string');
const {
  tickerForCurrency, stringToCurrency, currencyToString, convert, invertRate,
} = require('./currencies');

const MODE_BUY = 'buy';
const MODE_SELL = 'sell';
const MODE_TRADE = 'trade';

function longPairs(config, name) {
  const {
    fiat, crypto, methods, modes = [], medium, requirements,
  } = config;
  let pairs = [];
  const fiatParams = {
    service: name,
  };
  if (requirements) fiatParams.requirements = requirements;
  modes.forEach((mode) => {
    fiatParams.params = { mode };
    if (medium) fiatParams.params.medium = medium;
    if (mode === MODE_TRADE) return;
    pairs = [...pairs, ...fiatPairs(fiatParams, {
      fiatList: fiat,
      cryptoList: crypto,
      methodsList: methods,
    })];
  });
  return pairs;
}

function fiatPairs(fiatParams, { fiatList, cryptoList, methodsList }) {
  return fiatList.reduce((fiatPairs, fiat) => {
    let to; let
      from;
    if (fiatParams.params.mode === MODE_BUY) from = fiat;
    if (fiatParams.params.mode === MODE_SELL) to = fiat;
    return [...fiatPairs, ...cryptoList.reduce((cryptoPairs, crypto) => {
      if (fiatParams.params.mode === MODE_BUY) to = crypto;
      if (fiatParams.params.mode === MODE_SELL) from = crypto;
      return [...cryptoPairs, ...methodsList.reduce((methodPairs, method) => [...methodPairs, {
        from,
        to,
        type: method,
        ...fiatParams,
      }], [])];
    }, [])];
  }, []);
}

function configurePairs(pairConfig, name) {
  if (Array.isArray(pairConfig)) {
    let pairs = [];
    pairConfig.forEach((config) => {
      pairs = [...pairs, ...longPairs(config, name)];
    });
    return pairs;
  }
  return longPairs(pairConfig, name);
}

class Methods {
  constructor({ partner = '', pairConfig = {} } = {}) {
    this.name = partner;
    this.pairConfig = pairConfig;
    this.pairs = configurePairs(this.pairConfig, this.name);
  }

  config(override) {
    for (const option in override) {
      this[option] = override[option];
    }
  }

  // use this directly as an rpc-method or override

  async quote(params) {
    // 400 validations
    const errorString = this.requireParams(params, 'from', 'to', 'amount');
    if (errorString) { return errorString; }

    const invalidPair = this.validatePair(params);
    if (invalidPair) { return invalidPair; }

    return await this._fetchQuote(params);
  }

  // use this as a middleware
  upperCaseParams(params) {
    let value;
    const upperCasedParams = Object.keys(params).reduce((accumulator, param) => {
      value = params[param];
      if (value && value.currency) { value.currency = value.currency.toUpperCase(); }
      accumulator[param] = value;
      return accumulator;
    }, {});

    ['to', 'from'].forEach((param) => {
      if (upperCasedParams[param] && typeof upperCasedParams[param] === 'string') { upperCasedParams[param] = upperCasedParams[param].toUpperCase(); }
    });
    return upperCasedParams;
  }

  // helpers
  transferModeFor({ from, to }) {
    const pair = this.pairs.find((p) => p.from === from && p.to === to);
    return pair && pair.params.mode || MODE_BUY;
  }

  currencyObject(currencyObject, { amount, currency }) {
    if (typeof currencyObject !== 'undefined' && currencyObject.currency) return currencyObject;
    return stringToCurrency(amount, currency);
  }

  generatePassword(length) {
    return cryptoRandomString({ length });
  }

  // formatters

  quoteScreen({
    to, from, toAmount, toCurrency, fromAmount, fromCurrency, rate, fee,
  }) {
    if (!rate) return console.error('Rate required for quote');
    from = this.currencyObject(from, { amount: fromAmount, currency: fromCurrency });
    to = this.currencyObject(to, { amount: toAmount, currency: toCurrency });
    const mode = this.transferModeFor({ from: from.currency, to: to.currency });
    const fiat = (mode == MODE_BUY) ? from : to;
    const crypto = (mode == MODE_SELL) ? from : to;
    rate = (mode == MODE_BUY) ? invertRate(rate) : rate;
    const cryptoAmountInFiat = convert(crypto, fiat.currency, rate);

    if (!fee) {
      let feeInFiat = currencyToString(fiat) - currencyToString(cryptoAmountInFiat);
      if (mode == MODE_SELL) feeInFiat = currencyToString(cryptoAmountInFiat) - currencyToString(fiat);
      fee = stringToCurrency(feeInFiat, fiat.currency);
    }

    return {
      cryptoAmount: crypto,
      cryptoAmountInFiat,
      fiatAmount: fiat,
      fee,
    };
  }

  /**
     validators
  */

  requireParams(params, ...rest) {
    if (!rest.length) { rest = Object.keys(params); }
    const badParams = rest.reduce((accumulator, param) => {
      if (!params[param]) { accumulator.push(`'${param}'`); }
      return accumulator;
    }, []).join(', ');
    return badParams ? { error: { message: `Incomplete parameters, missing ${badParams} data` } } : null;
  }

  validatePair({ from, to, amount }) {
    const fromTicker = tickerForCurrency(from);
    const toTicker = tickerForCurrency(to);
    if (amount && (!amount.currency || !Number(amount.numerator) || !Number(amount.denominator))) {
      return { error: { message: 'Amount must have a currency, numerator, denominator' } };
    }

    if (amount && amount.currency !== fromTicker && amount.currency !== tickerForCurrency(to)) {
      return { error: { message: 'Amount must be in from or to currency.' } };
    }

    const { pairs } = this;
    const foundPair = pairs.some((p) => p.from === tickerForCurrency(from) && p.to === tickerForCurrency(to));
    if (!foundPair) {
      return { error: { message: 'Unsupported from or to.' } };
    }

    return null;
  }

  btcMainnet({ to, address }) {
    if (to.currency === 'BTC' && ['n', 'm'].includes(address[0])) {
      return { error: { message: 'bitcoin address must be mainnet' } };
    }
    return null;
  }
}
module.exports = Methods;
