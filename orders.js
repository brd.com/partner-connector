// Mostly here to store the state types, but also provides a helper
// function to validate items.

module.exports = {
  STATES: {
    COMPLETE: 'complete',
    CANCELLED: 'cancelled',
    REJECTED: 'rejected',
    EXPIRED: 'expired',
    FAILED: 'failed',
    PROCESSING: 'processing',
    AWAITING_TRANSFER_IN: 'awaiting_transfer_in',
    REVIEWING: 'reviewing',
  },

};
