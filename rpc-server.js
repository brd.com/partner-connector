const tracer = require('@brd.com/node-app/tracer');

const bodyParser = require('body-parser');
const ERRORS = require('./rpc-errors');

const denamespaced = (name) => {
  const m = name.match(/[^\/]*?$/);
  return m && m[0] || name;
};

const shortId = function (pattern = 'LDDLLL') {
  const sets = {
    // I and O omitted for readability:
    L: 'ABCDEFGHJKLMNPQRSTUVWXYZ',
    // 0 and 1 omitted for readability:
    D: '23456789',
  };

  const random = (set) => set[Math.floor(Math.random() * set.length)];
  return pattern.replace(/(L|D)/g, (m, k) => random(sets[k]));
};

module.exports = function (options) {
  if (!options.methods) {
    throw new Error('No methods specified for RPC Server');
  }

  const rpcMethods = options.methods;
  const rpcMiddleware = options.middleware || [];

  return [
    bodyParser.urlencoded({ extended: true }),
    bodyParser.json(),
    async function (req, res) {
      // first run through middleware stack, then call through to actual methods:
      function caller(ctx, middleware) {
        return async function () {
          const fn = middleware[0];

          if (typeof fn !== 'function') {
            throw new Error('middlewares must be functions.');
          }

          const next = caller(ctx, middleware.slice(1));

          try {
            const res = await fn.call(ctx, next);

            if (res) {
              if (res.result || res.error) {
                ctx.result = res.result;
                ctx.error = res.error;
              } else {
                ctx.result = res;
              }
            }
          } catch (x) {
            if (ctx.req.Sentry) {
              const key = shortId();

              ctx.req.Sentry.configureScope((scope) => {
                scope.setTag('key', key);
                scope.setExtra('error', ctx.error);
              });

              ctx.req.Sentry.captureException(x);

              if (ctx.hasOwnProperty('error')) {
                ctx.error.key = key;
              } else {
                ctx.error = { key };
              }
              x.key = key;
            }

            ctx.error = ctx.error || x;
          }
        };
      }

      const ctx = Object.create(rpcMethods);

      ctx.req = req;
      ctx.res = res;

      async function prepare(next) {
        if (this.req.method != 'POST'
           || !this.req.body
           || typeof this.req.body !== 'object'
           || !this.req.body.method) {
          this.res.status(400).json({
            jsonrpc: '2.0',
            error: {
              code: ERRORS.INVALID_REQUEST,
              message: 'This is an RPC endpoint, try using POST with an object body',
            },
          });
          return;
        }

        this.Sentry = this.req.Sentry;
        this.console = this.req.console;

        this.id = this.req.body.id;
        this.method = this.req.body.method;
        this.params = this.req.body.params;

        this.$t = this.req.$t;

        try {
          await next();
        } catch (x) {
          const key = shortId();

          if (this.req.Sentry) {
            this.req.Sentry.configureScope((scope) => {
              scope.setTag('key', key);
              scope.setExtra('error', this.error);
            });

            this.req.Sentry.captureException(x);
          }

          this.console.error('ERR: ', key, x, x.stack);

          // report to Sentry!
          this.res.json({
            error: {
              code: ERRORS.INTERNAL_ERROR,
              message: 'Internal error',
              key,
              data: process.env.NODE_ENV == 'development' ? x.stack.toString().split('\n') : undefined,
            },
          });

          return;
        }

        if (this.error instanceof Error) {
          if (process.env.NODE_ENV == 'development') {
            this.console.error('ERR: ', this.error, this.error.stack);

            this.res.json({
              error: {
                code: -32603,
                data: { stack: this.error.stack, error: String(this.error) },
              },
              id: this.id,
              jsonrpc: '2.0',
            });
          } else {
            let sentryId = null;
            const key = this.error.key || shortId();

            if (this.req.Sentry) {
              this.req.Sentry.configureScope((scope) => {
                scope.setTag('key', key);
                if (!(this.error instanceof Error)) {
                  scope.setExtra('error', this.error);
                }
              });

              if (this.error instanceof Error) sentryId = this.req.Sentry.captureException(this.error);
              else if (typeof this.error === 'string') sentryId = this.req.Sentry.captureMessage(this.error);
              else sentryId = this.req.Sentry.captureMessage('error-object');
            } else {
              sentryId = 'not available';
            }

            this.console.error(`Sentry ${sentryId} ${key}: `, this.error);

            this.res.json({
              error: {
                code: -32603,
                message: `exception ${key} in sentry`,
                key,
              },

              id: this.id,
              jsonrpc: '2.0',
            });
          }
        } else if (this.error) {
          this.res.json({ error: { code: -32603, ...this.error }, id: this.id, jsonrpc: '2.0' });
        } else {
          this.res.json({ result: this.result, id: this.id, jsonrpc: '2.0' });
        }
      }

      async function execute() {
        let method;

        method = rpcMethods[this.method];
        method = method || rpcMethods[denamespaced(this.method)];

        if (method) {
          if (typeof method === 'function') {
            const span = tracer.createChildSpan({ name: this.method });
            const ret = await method.call(this, this.params);
            span.endSpan();

            return ret;
          }
          this.error = { code: ERRORS.METHOD_NOT_FOUND, message: `Method ${this.method} must be a function.` };
        } else {
          this.error = { code: ERRORS.METHOD_NOT_FOUND, message: `Method ${this.method} not found.` };
        }
      }

      await caller(ctx, [prepare, ...rpcMiddleware, execute]).call(ctx);
    },
  ];
};
