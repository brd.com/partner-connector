import net from 'unet';
import { v4 as uuidv4 } from 'uuid';
import Vue from 'vue';

export default async (ctx, inject) => {
  inject('rpc', async (method, params) => {
    const { result, error, response } = await net({
      method: 'POST',
      url: `${ctx.env.appPath || ''}/rpc`,
      body: {
        id: uuidv4(), jsonrpc: '2.0', method, params,
      },
    });

    if ('jsonrpc' in result) {
      return {
        response,
        ...result,
      };
    }

    if (error) return { error };
    return { result };
  });
};
