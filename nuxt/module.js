const path = require('path');

module.exports = function ConnectorModule(moduleOptions) {
  this.nuxt.hook('build:extendRoutes', (routes, resolve) => {
    routes.push({
      path: '/_close',
      name: 'close',
      component: resolve(__dirname, 'pages/_close.vue'),
    });
  });

  this.addPlugin(path.resolve(__dirname, './plugins/rpc.js'));
};
