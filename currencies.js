const BN = require('bignumber.js');

BN.config({ EXPONENTIAL_AT: [-1e+9, 1e+9] });

const SCALES = {
  btc: 8,
  cad: 2,
  dkk: 2,
  eur: 2,
  gbp: 2,
  jpy: 2,
  usd: 2,
  rub: 2,
  aud: 2,
  krw: 2,
  chf: 2,
  sek: 2,
  ils: 2,
};

const SYMBOLS = {
  cad: '$',
  eur: '€',
  gbp: '£',
  usd: '$',
  aud: '$',
  jpy: '¥',
  krw: '₩',
  sek: 'kr',
  ils: '₪',
};

function scaleFor(currency) {
  // default to 18 for ETH and similar:
  return SCALES[currency.toLowerCase()] || 18;
}

function symbolFor(currency) {
  return SYMBOLS[currency.toLowerCase()] || '';
}

module.exports = {
  scaleFor,
  symbolFor,
  currencyToString(obj) {
    const num = new BN(obj.numerator);
    const den = new BN(obj.denominator);

    return num.dividedBy(den).toString();
  },
  stringToCurrency(str, currency) {
    const val = new BN(str || 0);

    const scale = scaleFor(currency);
    let digits = scale;

    if (typeof str === 'string') {
      const m = str.match(/^-?(\d*)(?:.(\d*))$/);
      const afterDot = m[2] || '';

      digits = Math.max(scale, afterDot.length);
    }

    const denominator = new BN(`1${'0'.repeat(digits)}`);
    const numerator = val.multipliedBy(denominator).integerValue();

    return {
      numerator: numerator.toFixed(),
      denominator: denominator.toFixed(),
      currency,
      scale,
    };
  },
  tickerForCurrency(obj) {
    if (typeof obj === 'string') return obj.toUpperCase();
    return obj.currency && obj.currency.toUpperCase();
  },

  convert(amount, to, rate) {
    if (typeof rate === 'undefined') return console.error('rate required to convert');
    if (amount.currency.toLowerCase() == to.toLowerCase()) {
      return amount;
    }

    const denominator = 1 + '0'.repeat(scaleFor(to));
    const numerator = ((amount.numerator / amount.denominator) * rate * denominator).toFixed(0);

    return { numerator, denominator, currency: to };
  },

  invertRate(rate) {
    return (1 / rate).toString();
  },

};
