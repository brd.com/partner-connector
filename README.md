# @brd.com/partner-connector

The BRD Partner Connector module provides tools for creating and
managing partner connector repos.  These tools include:

- An RPC Server

- Actions Helpers (functions to help you create valid action objects)

- Screens Helpers (functions to help you create valid screen objects)


## Plain Node Partner Connectors

Start by creating a node-app shell:

```
npx @brd.com/node-app create <name> <directory>
```

or a Nuxt shell:

```
npx @brd.com/node-app create-nuxt <name> <directory>
```

Install partner-connector (may need to do this inside of docker):

```
npm i --save @brd.com/partner-connector
```

You now will have a server/index.js with very little functionality.
Add a partner-connector RPC stack like so:

```
const express = require('express');
const app = express();

const RPC = require('@brd.com/partner-connector/rpc-server');

app.use('/example/rpc',RPC({
  middleware: [
    async function(next) {
      this.console.log(`rpc - ${this.method} changed to availability`);
      this.method = 'availability';

      return await next();
    },
  ],
  methods: {
    async availability(params) {
      this.result = { foo: this.params||'bar' };

      // or:

      return { foo: params || 'bar' };
    }
  }
}));

module.exports = app;
```

If you're using a Nuxt-based connector, you automatically get the
`/_close` route, designed to be the onClose parameter fed to the
_browser endpoint.

## Middleware

In order to be able to build reasonable, functional JSON-RPC services,
it's important to be able to abstract "steps" in the request handling
pipeline.  We've used a Koa-like mechanism for middleware. Every step
in the middleware chain can do anything to the response, re-route it,
cancel it, etc.  Hand off "down-chain" by calling `await next()`, and
you can manipulate output after that call by editing `this.result` or
`this.error`.

## Methods:

Methods in the JSON-RPC stack are what eventually handle all the
calls.  You can route calls using middleware, but eventually they're
handled by name in the `methods` hash (in a complex connector, this
should probably be in its own file).  Return the result or set
`this.result` to the result you wish to be sent down-wire.  Params are
the first argument passed to the function, or you can use
`this.params`.

## RPC Context Object

The RPC context object (`this` in middleware and methods) includes the
following:

- `req`: the express request object (from node-app)
- `res`: the express response object
- `id`,`method`,`params`: from the JSON-RPC request
- `$t`: translation function if it is supported
- `result`: the result that will be sent to the client
- `error`: the error that will be sent to the client

## Deployment

Partner connectors use a somewhat different deployment strategy, make
sure you set up your ingress TLS secrets to match the ones we already
are using for other partners:

```js
// deploy/ingress.js:

module.exports = function(hash) {
  if(['deploy-prod','deploy-stage'].includes(process.env.CI_JOB_STAGE)) {
    hash.spec.tls[0].secretName = `${process.env.CI_ENVIRONMENT_SLUG}-tls`;
  } else {
    hash.spec.tls[0].secretName = `wildcard-partners-tls`;
  }

  return hash;
};
```

You may also want to make sure your review environment names are
unique (example for coinberry):

```yaml
// deploy-review
deploy-review:
  stage: deploy-review
  <<: *global
  image: $DEPLOYER_IMAGE
  environment:
    name: cbrry-$CI_BUILD_REF_NAME
    url: https://$CI_COMMIT_REF_SLUG.partner.brd.tools/coinberry
    on_stop: stop-review

// stop-review:
stop-review:
  stage: stop-review
  <<: *global
  image: $DEPLOYER_IMAGE
  when: manual
  environment:
    name: cbrry-$CI_BUILD_REF_NAME
    url: https://$CI_COMMIT_REF_SLUG.partner.brd.tools/coinberry
    action: stop
```

## Changelog

List of breaking changes per-version:

#### 0.4

- Adapted the currencies object to be able to create high-precision
  fractional things - e.g. to represent 0.001 USD or 0.000000001 BTC
  (mostly for exchange rates).
