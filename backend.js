const net = require('unet');
const { v4: uuid } = require('uuid');
const crypto = require('crypto');

const Sentry = require('@sentry/node');
const { PrivateKey, signature, Base58 } = require('./lib/brd-node');

module.exports = function (options = {}) {
  const { baseURL, privateKeyString, deviceID } = {
    baseURL: process.env.BRD_API || 'https://stage2.breadwallet.com',
    privateKeyString: process.env.BRD_PRIVATE_KEY || (new PrivateKey()).toString(),
    deviceID: process.env.BRD_DEVICE_ID || uuid(),
    ...options,
  };

  this.console.log(`The backend has been set to ${baseURL}`);

  const privateKey = new PrivateKey(privateKeyString);

  let token = options.token || process.env.BRD_API_TOKEN;

  const HMAC_PATHS = [
    '/external/exchange/webhook',
  ];

  const BACKEND_SECRETS = {
    'api.breadwallet.com': process.env.PRODUCTION_BACKEND_WEBHOOK_SECRET,
    'stage2.breadwallet.com': process.env.STAGING_BACKEND_WEBHOOK_SECRET,
  };

  const webhookSecret = options.webhookSecret || BACKEND_SECRETS[(new URL(baseURL)).hostname] || '';

  const hmac = (data, secret) => crypto.createHmac('sha512', secret).update(data).digest('hex');

  async function prepare(options, next) {
    const url = new URL(options.url, baseURL);

    if (!HMAC_PATHS.includes(url.pathname)) {
      // use token-based "wallet" authentication:
      // make sure we have a token:
      if (!token) {
        this.console.log('NON-HMAC path, getting TOKEN for ', url.pathname);
        const { result, error, response } = await net({
          method: 'POST',
          baseURL,
          url: '/token',
          body: {
            pubKey: Base58.encode(privateKey.toPublicKey().toBuffer()),
            deviceID,
          },
        });

        if (error) return { error, response };
        if (!result.token) {
          throw new Error('invalid successful response - no token!');
        }

        token = result.token;
      }
    }

    // and make sure we have a date header:

    const headers = {
      date: (new Date()).toUTCString(),
      ...options.headers,
    };

    return await next({ baseURL, ...options, headers });
  }

  async function sign(options, next) {
    const url = new URL(options.url, baseURL);

    const headers = {
      ...options.headers,
    };

    if (HMAC_PATHS.includes(url.pathname)) {
      headers['x-webhook-signature'] = hmac(options.body, webhookSecret);
    } else {
      headers.authorization = signature(privateKey, token, options);
    }

    return await next({ ...options, headers });
  }

  return net({ middleware: [prepare].concat(net.DEFAULT_MIDDLEWARE, sign) }, true);
};
