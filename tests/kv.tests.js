const test = require('tape');
const kv = require('../kv-store');

test('kv get empty', async (T) => {
  const { etag, result } = await kv.get('foo');
  T.equal(etag, '0', 'etag should be zero');
  T.equal(result, undefined, 'result should be undefined');
  T.end();
});

test('kv string roundtrip', async (T) => {
  await kv.set('foo', 'bar');
  const { etag, result } = await kv.get('foo');

  T.equal(etag, '1', 'etag should be incremented to 1');
  T.equal(result, 'bar', 'result should be what we set');

  T.end();
});
