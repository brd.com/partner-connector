const test = require('tape');
const Currencies = require('../currencies');

const s2c = Currencies.stringToCurrency;
const c2s = Currencies.currencyToString;

test('some number conversions', (T) => {
  T.deepEqual(s2c(10.5, 'usd'), {
    numerator: '1050', denominator: '100', currency: 'usd', scale: 2,
  });
  T.deepEqual(s2c(10.501, 'usd'), {
    numerator: '1050', denominator: '100', currency: 'usd', scale: 2,
  });
  T.deepEqual(s2c(2000.501, 'usd'), {
    numerator: '200050', denominator: '100', currency: 'usd', scale: 2,
  });

  T.end();
});

test('some string conversions', (T) => {
  T.deepEqual(s2c('10.5', 'usd'), {
    numerator: '1050', denominator: '100', currency: 'usd', scale: 2,
  });
  T.deepEqual(s2c('10.501', 'usd'), {
    numerator: '10501', denominator: '1000', currency: 'usd', scale: 2,
  });
  T.deepEqual(s2c('2000.5012', 'usd'), {
    numerator: '20005012', denominator: '10000', currency: 'usd', scale: 2,
  });

  T.end();
});
