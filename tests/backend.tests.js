const test = require('tape');
const net = require('unet');
const backend = require('../backend');

test('health', async (T) => {
  const { result, error, response } = await backend({ url: '/health' });
  T.notOk(error);

  const text = await response.text();
  T.ok(text, 'must return some text');
  T.end();
});

test('authenticated-me', async (T) => {
  const { result, error } = await backend({ method: 'GET', url: '/me' });
  T.equal(typeof result.id, 'string');
  T.end();
});

test('unauthenticated-me', async (T) => {
  const { response, result, error } = await net({
    method: 'GET',
    baseURL: process.env.BRD_API || 'https://api.breadwallet.com',
    url: '/me',
  });

  T.equal(response.status, 401);
  T.end();
});
