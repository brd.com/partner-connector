const test = require('tape');
const encryption = require('../lib/encryption');

test('e2e encryption', (T) => {
  const start = 'foo bar baz';
  const encrypted = encryption.encrypt(Buffer.from(start, 'utf-8'));
  const decrypted = encryption.decrypt(encrypted).toString('utf-8');

  T.equal(start, decrypted, 'Start and decrypted text should be the same');
  T.notEqual(start, encrypted.toString('utf-8'), 'ENcrypted text should not be the same');

  T.end();
});
