const backendConfigatron = require('./backend');
const { encrypt, decrypt } = require('./lib/encryption');

async function head(key, backendUrl) {
  const Backend = backendConfigatron({ baseUrl: backendUrl });
  const { error, response } = await Backend({
    url: `/kv/1/${key}`,
    method: 'HEAD',
  });

  if (response && response.headers && 'etag' in response.headers) {
    return { etag: response.headers.etag, response };
  }

  return { error: error || 'network error', response };
}

async function get(key, backendUrl) {
  console.log(`KVStore.get for key: ${key} to url: ${backendUrl}`);
  const h = await head(key, backendUrl);
  if (h.error) {
    return { error: h.error };
  }

  const Backend = backendConfigatron({ baseUrl: backendUrl });
  const { result, error, response } = await Backend({
    url: `/kv/1/${key}`,
    method: 'GET',
    headers: {
      'if-none-match': h.etag,
      accept: 'application/octet-stream',
    },
  });

  // 404 is normal,
  if (response.status == 404) return { result: undefined, response, etag: h.etag };

  // otherwise report the error:
  if (error) return { error, response };

  return {
    etag: response.headers.etag,
    result: JSON.parse(decrypt(await response.body()).toString('utf-8')),
  };
}

async function set(key, value, backendUrl) {
  console.log(`KVStore.set for key: ${key} to url: ${backendUrl}`);

  const h = await head(key, backendUrl);
  if (h.error) {
    return { error: h.error };
  }

  const body = encrypt(Buffer.from(JSON.stringify(value), 'utf-8'));

  const Backend = backendConfigatron({ baseUrl: backendUrl });
  const { result, error, response } = await Backend({
    url: `/kv/1/${key}`,
    method: 'PUT',
    headers: {
      'if-none-match': h.etag,
      'content-type': 'application/octet-stream',
    },
    body,
  });

  if (!error) return { result: true };
  return { error };
}

async function del(key) {

}

module.exports = {
  get, set, del,
};
