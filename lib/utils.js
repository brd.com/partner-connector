const { stringify } = require('querystring');
const Actions = require('../actions');

const pause = (delay) => new Promise((res) => setTimeout(res, delay));

function get(string) {
  const parts = string.split('.');
  return function () {
    return parts.reduce((object, key) => object && object[key], this);
  };
}

function PromiseLatch() {
  this._res = [];
  this._rej = [];
}

PromiseLatch.prototype = {
  promise() {
    return new Promise((res, rej) => {
      this._res.push(res);
      this._rej.push(rej);
    });
  },
  resolve(...args) {
    if (this._res) {
      const call = this._res;
      this._rej = [];
      this._res = [];

      call.forEach((c) => c(...args));
    }
  },
  reject() {
    const args = Array.prototype.slice.call(arguments);
    if (this._rej) {
      const call = this._rej;
      this._rej = [];
      this._res = [];

      call.forEach((c) => c.apply(this, args));
    }
  },
};

function inheritProp(name) {
  return {
    inherited: true,
    get() {
      let p = this.$parent;
      while (p) {
        // if it has something, and it's not a computed property:
        if (name in p && !(p.$options.computed && name in p.$options.computed && p.$options.computed[name].inherited == true)) {
          return p[name];
        }

        p = p.$parent;
      }

      return null;
    },
    set(value) {
      let p = this.$parent;

      while (p) {
        if (name in p && !(p.$options.computed && name in p.$options.computed && p.$options.computed[name].inherited == true)) {
          return p[name] = value;
        }

        p = p.$parent;
      }

      return null;
    },
  };
}

function invertMap(map) {
  const r = {};
  for (const k in map) {
    const v = map[k];
    r[v] = k;
  }
  return r;
}

function downcase(a) {
  if (a && typeof a.toLowerCase === 'function') return a.toLowerCase();
  return a;
}

function upcase(string) {
  return string && (typeof string.toUpperCase === 'function') && string.toUpperCase() || string;
}

function caseInsensitiveCompare(a, b) {
  return downcase(a) === downcase(b);
}

function hashToUrlQuery(hash) {
  const r = [];
  for (const k in hash) {
    r.push(`${encodeURIComponent(k)}=${encodeURIComponent(hash[k])}`);
  }
  return r.join('&');
}

function getEnv() {
  return process.env.GITLAB_ENVIRONMENT_SLUG || process.env.SENTRY_ENV || process.env.NODE_ENV || 'dev';
}

function getKeyByValue(object, value) {
  return Object.keys(object).find((key) => object[key] === value);
}

class ServiceScreen {
  constructor({
    title, message, partner, event, type,
  }) {
    this.title = title;
    this.message = message;
    this.partner = partner;
    this.type = type;
    this.event = event;
    this.form = [];
    this.actions = [];
  }

  formItem(item) {
    this.form.push(item);
  }

  get formItems() {
    return this.form;
  }

  userAction(type, params, options) {
    if (!Actions[type]) console.error(`Invalid actions type ${type}`);

    if (type == 'rpc') {
      this.actions.push(Actions[type](params.method, params, options));
    } else {
      this.actions.push(Actions[type](params, options));
    }
  }

  get userActions() {
    return this.actions.length && this.actions;
  }

  get json() {
    return {
      partner: this.partner,
      type: 'screen',
      screenType: this.type,
      title: this.title,
      message: this.message,
      form: this.formItems,
      event: { name: `exchange.quote.${this.event}`, args: { serviceName: this.partner } },
      userActions: this.userActions,
    };
  }

  render() {
    console.info('deprecated. use json property.');
  }
}

class PartnerApi {
  constructor(baseURL, http) {
    this.baseURL = baseURL;
    this.http = http;
    return this.methodByName.bind(this);
  }

  async methodByName(methodName, params = {}) {
    const restMethod = params.method || 'get';
    let { baseURL } = this;
    baseURL += (restMethod == 'get' && Object.keys(params).length) ? `${methodName}?${stringify(params)}` : `${methodName}`;
    console.log(getEnv(), restMethod, baseURL, params);
    return await this.http[restMethod](baseURL, params);
  }
}

module.exports = {
  invertMap, downcase, caseInsensitiveCompare, hashToUrlQuery, getEnv, inheritProp, PromiseLatch, get, pause, getKeyByValue, upcase, ServiceScreen, PartnerApi,
};
