const crypto = require('crypto');

const ALGORITHM = 'aes256';
const KEY = process.env.BRD_PRIVATE_KEY || process.env.COOKIE_SECRET || 'insecure';

if (KEY.length < 10) {
  console.warn('WARNING: Insecure cookie secret...  set the environment variable BRD_PRIVATE_KEY or COOKIE_SECRET to get some actual security');
}

const ivLength = 16;

function keyFor(string) {
  return crypto.createHash('sha256').update(string).digest();
}

module.exports = {
  encrypt(buffer) {
    const iv = crypto.randomBytes(ivLength);

    const cipher = crypto.createCipheriv(ALGORITHM, keyFor(KEY), iv);
    let ciphered = cipher.update(buffer);

    ciphered = Buffer.concat([iv, ciphered, cipher.final()]);

    return ciphered;
  },
  decrypt(buffer) {
    const iv = buffer.slice(0, ivLength);
    const message = buffer.slice(ivLength);

    const decipher = crypto.createDecipheriv(ALGORITHM, keyFor(KEY), iv);
    let deciphered = decipher.update(message);
    deciphered = Buffer.concat([deciphered, decipher.final()]);

    return deciphered;
  },
};
