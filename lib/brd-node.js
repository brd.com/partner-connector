const { PrivateKey, crypto, encoding } = require('bitcore-lib');

const { ECDSA, Hash } = crypto;
const { Base58 } = encoding;

const { Buffer } = require('buffer');

const ALLOWED_METHODS = ['get', 'put', 'post', 'patch', 'delete', 'head'];

function sign(buffer, privateKey) {
  const hashbuf = Hash.sha256sha256(buffer);
  const ecdsa = new ECDSA();

  ecdsa.hashbuf = hashbuf;
  ecdsa.privkey = privateKey;
  ecdsa.pubkey = privateKey.toPublicKey();
  ecdsa.signRandomK();
  ecdsa.calci();

  return ecdsa.sig;
}

function pathFor(url) {
  const m = url.match(/^(?:.*?:\/\/.*?)?(\/.*?)$/);
  return m && m[1];
}

function signature(privateKey, token, hash) {
  const pk = new PrivateKey(privateKey);
  if (typeof token !== 'string' && token.length > 5) throw new Error('Token must be a long opaque string');

  const path = hash.path || pathFor(hash.url || hash.uri);
  const method = (hash.method || 'GET').toUpperCase();
  const headers = {};
  const body = hash.body || null;

  // ensure headers are lower-case so we can reliably get to them:
  for (const k in hash.headers) {
    headers[k.toLowerCase()] = hash.headers[k];
  }

  if (!path || typeof path !== 'string') throw new Error('URL or URI must be a string');
  if (typeof method !== 'string') throw new Error('Method must be a string');
  if (!ALLOWED_METHODS.includes(method.toLowerCase())) throw new Error('Method must be an HTTP method');

  // best way I know of to figure out if it's a valid date:
  if (isNaN((new Date(headers.date)).getTime())) throw new Error('Must specify a Date header');

  let bodySHA256 = '';
  if (body) {
    const sha = Hash.sha256(Buffer.from(body));
    bodySHA256 = Base58.encode(sha);
  }

  const parts = [
    method,
    bodySHA256,
    headers['content-type'] || '',
    headers.date,
    path,
  ];

  const signature = sign(Buffer.from(parts.join('\n')), privateKey);

  return `bread ${token}:${Base58.encode(signature.toCompact())}`;
}

module.exports = {
  signature, PrivateKey, Base58,
};
